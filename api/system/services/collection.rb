module Minutes
    class Collection
        @chrono = {
            hour: 0,
            min: 0,
        }

        def self.retrieve
            return @chrono 
        end
    
        def self.save(new_chrono)
            @chrono = new_chrono
        end
    end
end