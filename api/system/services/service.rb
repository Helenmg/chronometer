require_relative 'collection'

module Min
    class Service
        def self.add_min(minutes)
            chrono = Minutes::Collection.retrieve

            chrono[:min] += minutes

            Minutes::Collection.save(chrono)
        end
    end
end