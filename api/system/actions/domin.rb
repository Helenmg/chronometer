require_relative '../services/service'

module Actions
    class DoMin
        def self.do(min)
            Min::Service.add_min(min)
        end 
    end
end