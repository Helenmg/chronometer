require_relative '../system/actions/domin'
require_relative '../system/services/collection'


describe "chronometer" do
    it "have one minute" do
        min = 1
        Actions::DoMin.do(min)
        chrono = Minutes::Collection.retrieve

        expect(chrono[:min]).to eq(min)
    end
end
